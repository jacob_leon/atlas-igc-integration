import requests

class igc_api:
	
	def __init__(self, url):
		self.url = url

	def createAsset(self, payload):
		return self.__postAsset(payload)
		
	def updateAsset(self, payload):
		return self.__putAsset(payload)
		
	def retrieveAsset(self, id):	
		return self.__getAsset(id)
		
	def search(self, payload):
		return self.__getSearch(payload)
		
	def complexSearch(self, payload):
		return self.__postSearch(payload)
			
	def __postAsset(self, payload):
		return requests.post(self.url + '/asset/', json = payload)
		
	def __putAsset(self, payload):
		return requests.put(self.url + '/asset/', json = payload)
		
	def __getAsset(self, id):
		return requests.get(self.url + '/asset/' + id)
		
	def __getSearch(self, payload):
		return requests.get(self.url + '/search/', params = payload)
	
	def __postSearch(self, payload):
		return requests.post(self.url + '/search/', json = payload)


	def __checkErrors(self, resp):
		if resp.status_code != 200:
			# This means something went wrong.
			raise ApiError('GET ' + api.format(resp.status_code))
		
		