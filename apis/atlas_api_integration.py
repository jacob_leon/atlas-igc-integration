import requests

class atlas_api_integration:

	def __init__(self, url):
		self.url = url

	def getEntities(self, EntityType):
		return self.__getMetadata('/api/atlas/entities/?type=', EntityType)
	
	def getEntityDetails(self, Guid):	
		return self.__getMetadata('/api/atlas/entities/', Guid)	
			
	def __getMetadata(self, api, EntityType):
		#print(self.url + api + EntityType)
		resp = requests.get(self.url + api + EntityType)
		if resp.status_code != 200:
			# This means something went wrong.
			raise ApiError('GET ' + api.format(resp.status_code))
		return resp.json()
	