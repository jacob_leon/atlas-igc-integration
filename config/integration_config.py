import os, json

class integration_config:
	
	def __init__(self):
		script_path = os.path.dirname(os.path.abspath(__file__)) + '\config'
		# import and parse JSON config file
		config_file = open(script_path, encoding='utf-8')
		self.config_dict = json.loads(config_file.read())
		config_file.close()
	#	print(self.config_dict)
		
	def getIGCUrl(self):
		return self.config_dict["igc_url"]
		
	def getAtlasUrl(self):
		return self.config_dict["atlas_url"]